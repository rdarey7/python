mypath= '/export/i2e/logs'

import json
import os

towrite = []

def readlog(file):
    try:
        with open(file) as fl:
            content = fl.readlines()
            return content

    except:
        content = []
    return content
from os import listdir
from os.path import isfile, join
onlyfiles = [f for f in listdir(mypath) if (isfile(join(mypath, f)))]

for elt in onlyfiles:
    if elt.endswith(".json"):

        content= readlog(os.path.join(mypath,elt))
        for elt in content:
            temp = json.loads(elt)
            towrite.append(temp)
        break

import csv
keys = towrite[0].keys()
with open('Logs_int_csv.csv', 'w')  as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(towrite)


