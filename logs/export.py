mypath= '/Users/raymondfasasi/Desktop'

import json
import os
from os import listdir
from os.path import isfile, join
import csv
import datetime

towrite =[]

def readlog(file):
    try:
        with open(file) as fl:
            content = fl.readlines()
            return content

    except:
        content = []
    return content

onlyfiles = [f for f in listdir(mypath) if (isfile(join(mypath, f)))]
keys = []
for elt in onlyfiles:
    if elt.endswith(".json"):

        content = readlog(os.path.join(mypath,elt))
        for elt in content:
            try:
                temp = json.loads(elt)
                
                for t in temp.keys():
                    if(t not in keys) :
                        keys.append(t)
                
                towrite.append(temp)
            except:
                continue

today = datetime.datetime.now().strftime("%x")
fileName = 'Log_' + str(today) + '.csv'

with open(fileName, 'w')  as output_file:
    dict_writer = csv.DictWriter(output_file, fieldnames=keys)
    dict_writer.writeheader()
    dict_writer.writerows(towrite)
