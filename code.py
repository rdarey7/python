mypath= '/export/i2e/logs'

import json
import os

try:
    with open('Logs_int_csv.csv' ) as fl:
        temp = fl.readlines()
        towrite = []
        for elt in temp:
            try:
                ex =elt.strip().split(',')
                print(ex)
                dc={}
                dc['time'] =ex[0]
                dc['remote_ip'] = ex[1]
                dc['remote_user'] = ex[2]
                dc['request'] = ex[3]
                dc['response'] = ex[4]
                dc['bytes'] = ex[5]
                dc['referrer'] = ex[6]
                dc['agent'] = ex[7]
                towrite.append(dc)
            except:
                continue
except:
    towrite =[]

def readlog(file):
    try:
        with open(file) as fl:
            content = fl.readlines()
            return content

    except:
        content = []
    return content
from os import listdir
from os.path import isfile, join
onlyfiles = [f for f in listdir(mypath) if (isfile(join(mypath, f)))]

for elt in onlyfiles:
    if elt.endswith(".json"):

        content= readlog(os.path.join(mypath,elt))
        for elt in content:
            print(elt)
            temp = json.loads(elt)
            towrite.append(temp)
        break

import csv
keys = towrite[0].keys()
with open('Logs_int_csv.csv', 'w', newline='')  as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(towrite)


