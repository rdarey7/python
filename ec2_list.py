#!/usr/bin/python
import boto3
import pprint
session=boto3.session.Session(profile_name="default")
ec2_con_re=session.resource(service_name="ec2",region_name="us-east-1")
ec2_con_cli=session.client(service_name="ec2",region_name="us-east-1")
#--------------------------
#This is the interactive code and will accept valid tags and values in AWS environment. Also, the machine needs to have boto3 and python installed. 


tagg = input("Enter the tag name: ")
val = input("Enter the Values name: ")
filter = [
    {'Name': 'tag:' + tagg, 'Values': [val]}
]


for each_info in ec2_con_cli.describe_instances(Filters=filter)['Reservations']:
# for each_info in ec2_con_cli.describe_instances()['Reservations']
   for each_inst in each_info['Instances']:
       #print each_inst['InstanceId'], each_inst['InstanceType']
        #print(each_inst['InstanceId'], each_inst['InstanceType'])
       print("\nID: {0}\nType: {1}\nAMI: {2}\nState {3}\n----------------".format(each_inst['InstanceId'], each_inst['InstanceType'], each_inst['ImageId'], each_inst['State']))
    
        
