#!/usr/bin/python
import boto3
import pprint
session=boto3.session.Session(profile_name="default")
ec2_con_re=session.resource(service_name="ec2",region_name="us-east-1")
ec2_con_cli=session.client(service_name="ec2",region_name="us-east-1")
#--------------------------
#This is hardcoded and will only accept tag:Owner and Value: TeamA except changed to a valid tag and value in client environment. Also boto3 and python needs to be installed 



for each_info in ec2_con_cli.describe_instances(Filters=[{'Name' : 'tag:Owner','Values' : ['TeamA']}])['Reservations']:
   for each_inst in each_info['Instances']:
      print("\nID: {0}\nType: {1}\nAMI: {2}\nState {3}\n----------------".format(each_inst['InstanceId'], each_inst['InstanceType'], each_inst['ImageId'], each_inst['State']))
